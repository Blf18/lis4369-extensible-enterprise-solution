# LIS4369 - Extensible Enterprise Solutions

## Brandt Frazier-Allen

### Assignment 4 Requirements:

*Development*

1. Code and run demo.py 
2. Then use demo.py to backyard-engineer the screenshots below it.
3. When displaying the required graph, answer the following question: Why is the graph line split?

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshot as per example below.
* Upload A4.ipynb file and create link in README.md;
	* Restart & Clear Output
	* Restart & Run All

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment 4 Screenshots:

*Screenshot of demo.py running* :

Demo 1             |  Demo 2  
:-------------------------:|:-------------------------:
![Demo 1 Screenshot](img/demo_1.png)  |  ![Demo 2 Screenshot](img/demo_2.png)

*Screenshot of Data Analysis 2 running* :

Data Analysis 2           |  Data Analysis 2           |  Graph   
:-------------------------:|:-------------------------:|-------------------------:
![Running Data Analysis 2 Screenshot](img/dictionary_1.png)  |  ![Running Data Analysis 2 Screenshot](img/dictionary_2.png)  |  ![Data Analysis 2 Graph Screenshot](img/Dictionary_3.png)

*Screenshot of [Jupyter Notebook - Data Analysis 2](a4_data_analysis_2/a4_data_analysis_2.ipynb "Jupyter Notebook - Data Analysis 2")*:

Jupyter Notebook - Data Analysis 2             |  Jupyter Notebook - Data Analysis 2 |  Jupyter Notebook - Data Analysis 2 Running  
:-------------------------:|:-------------------------:|-------------------------:
![Jupyter Data Analysis 2 Screenshot](img/jup_1.png)  |  ![Jupyter Data Analysis 2 Screenshot](img/jup_2.png)  |  ![Graph Screenshot](img/jup_3.png)

*Screenshot of Python Skill Sets*:

Skill Set 10: Dictionaries           |  Skill Set 11: Random Numbers           |  Skill Set 12: Temperature Conversions   
:-------------------------:|:-------------------------:|-------------------------:
![Using Dictionary Screenshot](img/ss10_dictionaries.png)  |  ![Using Random Numbers Screenshot](img/ss11_random_nums.png)  |  ![Using Temperature Conversion Screenshot](img/ss12_temperature_conversion.png)