
# LIS4369 - Extensible Enterprise Solutions

## Brandt Frazier-Allen

### Project 2 Requirements:

*Development:*

1. Requirements:
	2. Review Assignment 5 screenshots as examples, and use references above for the following requirements:
	3. Backward-engineer the lis4369\_p2_requirements.txt file
	4. Be sure to include at least two plots (*must* include *your* name in plot titles), in your README.md file.

2. Be sure to test your program using RStudio.


#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshot of output.


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Project 2 Screenshots:

*Screenshot of Two Plots* :

Deplacement vs MPG             |  Weight vs MPG
:-------------------------:|:-------------------------:
![QPlot Screenshot](img/plot_disp_and_mpg_1.png)  |  ![Payroll With Overtime Screenshot](img/plot_disp_and_mpg_2.png)


*Screenshot of Project 2 Outputs (Beginning)*:

![Running P2 Screenshot](img/1.png)

*Screenshot of Project 2 Outputs (Middle)*:

![Running P2 Screenshot](img/2.png)

*Screenshot of Project 2 Outputs (Ending)*:

![Running P2 Screenshot](img/3.png)





