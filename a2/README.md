> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Brandt Frazier-Allen

### Assignment 2 Requirements:

*Development:*

1. Backward-engineer (using Python) the following screenshots:
2. The program should be organized with two modules(See Ch. 4):
	3. 	functions.py module contains the following functions:
		4. get_requirements()
		5. calculate_payroll()
		6. print_pay()

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots of Python skill sets
* Screenshots of payroll app
* Upload A2 .ipynb file and create link in README.md


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Payroll App*:

Payroll No Overtime             |  Payroll With Overtime
:-------------------------:|:-------------------------:
![Payroll No Overtime Screenshot](img/payroll_no_overtime.png)  |  ![Payroll With Overtime Screenshot](img/payroll_with_overtime.png)



*Screenshot of [Jupyter Notebook - Payroll App](a2_payroll/a2_payroll.ipynb "Jupyter Notebook - Payroll App")*:

Jupyter Notebook - Payroll App 1             |  Jupyter Notebook - Payroll App 2
:-------------------------:|:-------------------------:
![Jupyter Payroll No Overtime Screenshot](img/jupyter_payroll_1.png)  |  ![Jupyter Payroll With Overtime Screenshot](img/jupyter_payroll_2.png)

*Screenshot of Python Skill Sets*:

Skill Set 1: Square Feet to Acres           |  Skill Set 2: Miles Per Gallon           |  Skill Set 3: IT/ICT Student Percentage   
:-------------------------:|:-------------------------:|-------------------------:
![Square ft to Acres Screenshot](img/ss1_square_ft_to_acres.png)  |  ![Mile Per Gallon Screenshot](img/ss2_miles_per_gallon.png)  |  ![IT/ICT Student Percentage Screenshot](img/ss3_it_ict_student_percentage.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Blf18/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Blf18/myteamquotes/ "My Team Quotes Tutorial")
