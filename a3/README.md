> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Brandt Frazier-Allen

### Assignment 3 Requirements:

*Development:*

1. functions.py module contains the following functions:
	1. get_requirements()
	2. estimate\_painting_cost()
	3. print\_painting_estimate()
	4. print\_painting_percentage()
2. main.py module imports the functions.py module, and calls the functions.

#### README.md file should include the following items:

1. Assignment requirements, as per A1.
2. Provide screenshots of completed painting estimator
3. Provide screenshots of completed Jupyter Notebook
    	
   
			


#### Assignment Screenshots:

*Screenshot of Painting Estimator*:

![Painting Estimator Screenshot](img/a3_painting_estimator.png)

*Screenshot of [Jupyter Notebook - Painting Estimator](a3_painting_estimator/a3_painting_estimator.ipynb "Jupyter Notebook - Painting Estimator")*:

Jupyter Notebook - Painting Estimator 1           |  Jupyter Notebook - Painting Estimator 2           |  Jupyter Notebook - Painting Estimator 3   
:-------------------------:|:-------------------------:|-------------------------:
![Painting Estimator Code](img/jupyter_paint_1.png)  |  ![Painting Estimator Output Screenshot](img/jupyter_paint_3.png)  |  ![Painting Estimator Output 2 Screenshot](img/jupyter_paint_2.png)

*Screenshot of Python Skill Sets*:

Skill Set 4: Calorie Percentage           |  Skill Set 5: Selection Structures           |  Skill Set 6: Loops   
:-------------------------:|:-------------------------:|-------------------------:
![Calorie Percentage Screenshot](img/ss1.png)  |  ![Selection Structures Screenshot](img/ss2.png)  |  ![Loops Screenshot](img/ss3.png)
