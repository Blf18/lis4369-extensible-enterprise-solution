
# LIS4369 - Extensible Enterprise Solutions

## Brandt Frazier-Allen

### Project 1 Requirements:

*Development*

1. Code and run demo.py 
2. Updating conda, and installing Python packages.
3. Note: *Be sure* to do the following commands:
	4. Log in as administrator (PC: command prompt, LINUX/Mac:sudo)
	5. conda update conda
	6. conda update --all
	7. pip install pandas-datareader

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshot as per example below.
* Upload P1 .ipynbfile and create link in README.md;
	* Restart & Clear Output
	* Restart & Run All

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Project 1 Screenshots:

*Screenshot of demo.py running* :

![Demo Screenshot](img/demo.png)

*Screenshot of Data Analysis 1 running* :

Data Analysis 1             |  Data Analysis 1 Graph
:-------------------------:|:-------------------------:
![Data Analysis 1 Screenshot](img/data_analysis_1.png)  |  ![Payroll With Overtime Screenshot](img/data_analysis_2.png)

*Screenshot of [Jupyter Notebook - Data Analysis 1](p1_data_analysis_1/data_analysis_1.ipynb "Jupyter Notebook - Data Analysis 1")*:

Jupyter Notebook - Data Analysis 1             |  Jupyter Notebook - Data Analysis 1 Running
:-------------------------:|:-------------------------:
![Jupyter Data Analysis 1 Screenshot](img/jupyter_data.png)  |  ![Jupyter Data Analysis 1 Running Screenshot](img/jupyter_data_run.png)

*Screenshot of Python Skill Sets*:

Skill Set 7: Using Lists           |  Skill Set 8: Using Tuples           |  Skill Set 9: Using Sets   
:-------------------------:|:-------------------------:|-------------------------:
![Using Lists Screenshot](img/ss7_lists.png)  |  ![Using Tuples Screenshot](img/ss8_tuples.png)  |  ![Using Sets Screenshot](img/ss9_sets.png)



