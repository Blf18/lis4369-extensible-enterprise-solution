> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions (Python)

## Brandt Frazier-Allen

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
	- Install R
	- Install R Studio
	- Install Visual Studio Code
	- Create *a1\_tip_calculator* application
	- Create *a1 tip calculator* Jupyter Notebook
	- Provide screenshots of installations 
	- Create Bitbucket repo
	- Complete Bitbucket tutorial (bitbucketstationlocations)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Payroll app using "Seperation of Concerns" design principles
	- Provide screenshots of completed *Payroll Calulator*
	- Provide screenshots of completed Python Skill Sets 1, 2, and 3
	- Provide screenshots of completed Jupyter Notebook
2. [A3 README.md](a3/README.md "My A3 README.md file")
	- Assignment requirements, as per A1.
	- Provide screenshots of completed painting estimator
	- Provide screenshots of completed Jupyter Notebook
	- Provide screenshots of completed Python Skill Sets 4, 5, and 6

2. [A4 README.md](a4/README.md "My A4 README.md file")
	- Assignment requirements, as per A1.
	- Provide screenshots of completed Demo
	- Provide screenshots of completed Data Analysis 2
	- Provide screenshots of completed Jupyter Notebook
	- Provide screenshots of completed Python Skill Sets 10, 11, and 12

2. [A5 README.md](a5/README.md "My A5 README.md file")
	- Assignment requirements, as per A1.
	- Screenshot of output from A5 code, *and* from tutorial.
	- Complete the following tutorial: Introduction\_to\_R Setup\_and_Tutorial
	- Be sure to test your program using RStudio

*Projects:*

2. [P1 README.md](p1/README.md "My P1 README.md file")
	- Assignment requirements, as per A1.
	- Provide screenshots of completed Data Analysis
	- Provide screenshots of completed Jupyter Notebook
	- Provide screenshots of completed Python Skill Sets 7, 8, and 9

2. [P2 README.md](p2/README.md "My P2 README.md file")
	- Assignment requirements, as per A1.
	- Provide screenshots of two plots
	- Provide screenshots of Project 2 Output




