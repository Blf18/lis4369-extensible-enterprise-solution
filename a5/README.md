# LIS4369 - Extensible Enterprise Solutions

## Brandt Frazier-Allen

### Assignment 5 Requirements:

*Development*

1. Complete the following tutorial: Introduction\_to\_R Setup\_and_Tutorial
	2. 	Save as: learn\_to_use r.R 
	2. Code and run lis4369_a5.R
	3. Be sure to include at least two plots for each part.
4. Be sure to test your program using RStudio

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshot of output from code below, *and* from tutorial.


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment 5 Screenshots:
*Screenshot of Assignment 5 in RSTudio running* :

Four Windows           |  Console           |    
:-------------------------:|:-------------------------:|-------------------------:
![Four Windows Screenshot](img/four_windows.png)  |  ![Console of Code Screenshot](img/console.png)  | 

*Screenshot of Plots in RSTudio* :

Box Plot           |  Scatter Plot           |    
:-------------------------:|:-------------------------:|-------------------------:
![Four Windows Screenshot](img/boxplot.png)  |  ![Console of Code Screenshot](img/scatterplot.png)  | 

*Screenshot of Python Skill Sets*:

Skill Set 13: Sphere Volume Calculator           |  Skill Set 14: Calculator With Error Handling           |  Skill Set 15: File Write Read   
:-------------------------:|:-------------------------:|-------------------------:
![Using Sphere Volume Calculator Screenshot](img/sphere_volume.png)  |  ![Using Calculator with Error Handling Screenshot](img/error_handling.png)  |  ![Using Write Read File Screenshot](img/write_read.png)